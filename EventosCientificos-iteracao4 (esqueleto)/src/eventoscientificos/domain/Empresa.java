package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author Nuno Silva
 */

public class Empresa
{
    private RegistoUtilizadores m_regUtilizadores;
    private RegistoEventos m_regEventos;
    private RegistoTipoConflitos m_regTipoConflitos;
    private Timer m_timer;
    private List<MecanismoDecisao> m_listaMecanismoDecisao;
    private List<MecanismoDetecaoConflito> m_listaMecanismoDetecao;
    

        
    public Empresa()
    {
        m_regUtilizadores = new RegistoUtilizadores();
        m_regEventos = new RegistoEventos(); 
        m_regTipoConflitos = new RegistoTipoConflitos();
        m_timer = new Timer();
        m_listaMecanismoDecisao = new ArrayList<MecanismoDecisao>();
        m_listaMecanismoDetecao = new ArrayList<MecanismoDetecaoConflito>();
     
        //
        addDefaultData();
    }
    
    private void addDefaultData()
    {
        // Mecanismos de Detecao de Conflitos
        m_listaMecanismoDetecao.add(new MecanismoDetecaoConflito1());
        m_listaMecanismoDetecao.add(new MecanismoDetecaoConflito2());
    }

    public RegistoUtilizadores getRegistoUtilizadores()
    {
        return this.m_regUtilizadores;
    }
    
    public RegistoEventos getRegistoEventos()
    {
        return this.m_regEventos;
    }
    
    public RegistoTipoConflitos getRegistoTipoConflitos()
    {
        return this.m_regTipoConflitos;
    }
    
    public void schedule(TimerTask task, Date date)
    {
        m_timer.schedule(task, date);
    }
    
    public List<MecanismoDecisao> getMecanismosDecisao() 
    {
        List<MecanismoDecisao> lMec = new ArrayList<MecanismoDecisao>();
        
        for( ListIterator<MecanismoDecisao> it = m_listaMecanismoDecisao.listIterator(); it.hasNext(); )
        {
            lMec.add( it.next() );
        }
        
        return lMec;
    }
    
    public List<MecanismoDetecaoConflito> getMecanismosDetecaoConflito() 
    {
        List<MecanismoDetecaoConflito> lMec = new ArrayList<>();
        
        for( ListIterator<MecanismoDetecaoConflito> it = m_listaMecanismoDetecao.listIterator(); it.hasNext(); )
        {
            lMec.add( it.next() );
        }
        
        return lMec;
    }

    public RegistoTipoConflitos getRegistoDeConflitos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}