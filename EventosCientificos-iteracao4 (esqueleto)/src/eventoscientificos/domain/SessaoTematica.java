/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.domain;

import eventoscientificos.state.STState;
import eventoscientificos.state.STStateCriada;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class SessaoTematica implements CPDefinivel, Submissivel, Distribuivel, Licitavel, Revisavel
{
    private String m_codigo;
    private String m_descricao;
    private Date m_strDataInicioSubmissao;
    private Date m_strDataFimSubmissao;
    private Date m_strDataInicioDistribuicao;
    
    private ListaProponentes m_lsProp;
    private STState m_state;
    private CP m_cp;
    private ListaSubmissoes m_lstSubmissoes;
    private ProcessoLicitacao m_processoLicitacao;
    private ProcessoDecisao m_processoDecisao;
    private ProcessoDistribuicao m_processoDistribuicao;
    
    public SessaoTematica(String codigo, String descricao, Date dtInicioSub, Date dtFimSub, Date dtInicioDistr)
    {
        this.m_codigo = codigo;
        this.m_descricao = descricao;
        this.m_strDataInicioSubmissao = dtInicioSub;
        this.m_strDataFimSubmissao = dtFimSub;
        this.m_strDataInicioDistribuicao = dtInicioDistr;
        
        this.m_lsProp = new ListaProponentes();
        this.m_lstSubmissoes =new ListaSubmissoes(this);
        new STStateCriada(this);
    }

    public boolean isInRegistado()
    {
        return this.m_state.isInRegistado();
    }
    public boolean isInCPDefinida()
    {
        return this.m_state.isInCPDefinida();
    }
    public boolean isInEmSubmissao()
    {
        return this.m_state.isInEmSubmissao();
    }
    public boolean isInEmDetecaoConflitos()
    {
        return this.m_state.isInEmDetecaoConflitos();
    }
    public boolean isInEmLicitacao()
    {
        return this.m_state.isInEmLicitacao();
    }
    public boolean isInEmDistribuicao()
    {
        return this.m_state.isInEmDistribuicao();
    }
    public boolean isInEmRevisao()
    {
        return this.m_state.isInEmRevisao();
    }
    public boolean isInEmDecisao()
    {
        return this.m_state.isInEmDecisao();
    }
    public boolean isInEmDecidido()
    {
        return this.m_state.isInEmDecidido();
    }
    
    public boolean setStateRegistado()
    {
        return this.m_state.setStateRegistado();
    }
    public boolean setStateEmSubmissao()
    {
        return this.m_state.setStateEmSubmissao();
    }
    public boolean setStateEmDetecaoConflitos()
    {
        return this.m_state.setStateEmDetecaoConflitos();
    }
    public boolean setStateEmLicitacao()
    {
        return this.m_state.setStateEmLicitacao();
    }
    public boolean setStateEmDistribuicao()
    {
        return this.m_state.setStateEmDistribuicao();
    }
    public boolean setStateEmRevisao()
    {
        return this.m_state.setStateEmRevisao();
    }
    public boolean setStateEmDecisao()
    {
        return this.m_state.setStateEmDecisao();
    }
    public boolean setStateEmDecidido()
    {
        return this.m_state.setStateEmDecidido();
    }
    
    @Override
    public CP novaCP()
    {
         m_cp = new CPSessaoTematica();
        
        return m_cp;
    }

    @Override
    public boolean setCP(CP cp)
    {
        m_cp = cp;
        return true;
    }

    public boolean setState(STState stState)
    {
        this.m_state = stState;
        return true;
    }

    public boolean valida()
    {
        return this.m_state.valida();
    }

    public ListaProponentes getListaProponentes()
    {
        return this.m_lsProp;
    }
    
    public Date getDataInicioSubmissao()
    {
        return this.m_strDataInicioSubmissao;
    }
    
    public Date getDataFimSubmissao()
    {
        return this.m_strDataFimSubmissao;
    }
    
    public Date getDataInicioDistribuicao()
    {
        return this.m_strDataInicioDistribuicao;
    }

    @Override
    public ListaSubmissoes getListaSubmissoes()
    {
        return this.m_lstSubmissoes;
    }

    @Override
    public ProcessoLicitacao iniciaDetecao() {
        m_state.setStateEmDetecaoConflitos();
        return new ProcessoLicitacaoST();
    }

    @Override
    public List<Revisor> getRevisores() 
    {
        return m_cp.getRevisores();
    }

    public Licitacao novaLicitação(Revisor r, Submissao s) 
    {
	throw new UnsupportedOperationException();
    }

	
    public boolean addLicitacao(Licitacao l) 
    {
	throw new UnsupportedOperationException();
    }

    
    
    public void setProcessoLicitacao(ProcessoLicitacao pl) 
    {
        if (pl.valida())
        {
            m_processoLicitacao = pl;
            m_state.setStateEmLicitacao();
        }
    }
    

    @Override
    public ProcessoLicitacao getProcessoLicitacao() 
    {
        return m_processoLicitacao;
    }

    

    @Override
    public List<Submissao> getSubmissoes() 
    {
        return m_lstSubmissoes.getSubmissoes();
    }

    @Override
    public ProcessoDistribuicao getProcessoDistribuicao()
    {
        return m_processoDistribuicao;
    }

    @Override
    public void alteraParaEmDecisão() {
        ListaRevisoes lr=m_processoDistribuicao.getListaDeRevisoes();
        if (lr.isRevisoesConcluidas())
            setEmDecisao();
        
    }

    private boolean setEmDecisao() 
    {
        return this.m_state.setStateEmDecisao();
    }

   
}
