package eventoscientificos.domain;

/**
 *
 * @author Nuno Silva
 */
public class Utilizador
{
    private String m_strNome;
    private String m_strUsername;
    private String m_strPassword;
    private String m_strEmail;

    public Utilizador()
    {
    }
    
    protected Utilizador(String strNome,String strUsername, String strPwd, String strEmail)
    {
        setNome(strNome);
        setEmail(strEmail);
        setUsername(strUsername);
        setPassword(strPwd);
    }
    
    public String getNome()
    {
        return this.m_strNome;
    }
    
    public String getPwd()
    {
        return this.m_strPassword;
    }
    
    public String getUsername()
    {
        return this.m_strUsername;
    }
    
    public String getEmail()
    {
        return this.m_strEmail;
    }
    
    public void setNome(String strNome)
    {
        this.m_strNome = strNome;
    }

    public void setUsername(String strUsername)
    {
        m_strUsername = strUsername;
    }

    public void setPassword(String strPassword)
    {
        m_strPassword = strPassword;
    }

    public void setEmail(String strEmail)
    {
        this.m_strEmail = strEmail;
    }

    public boolean valida()
    {
        System.out.println("Utilizador:valida: " + this.toString());
        return true;
    }
    
    @Override
    public String toString()
    {
        String str = "Utilizador:\n";
        str += "\tNome: " + this.m_strNome + "\n";
        str += "\tUsername: " + this.m_strUsername + "\n";
        str += "\tPassword: " + this.m_strPassword + "\n";
        str += "\tEmail: " + this.m_strEmail + "\n";

        return str;
    }
    
    @Override
    public Utilizador clone()
    {
        return new Utilizador(this.getNome(),this.getUsername(),this.getPwd(),this.getEmail());
    }
            
}

