/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author iazevedo
 */
public class ProcessoDecisaoST {
    
    List<Decisao> m_listaDecisoes;
    MecanismoDecisao m_mecanismoDecisao;
    
    public ProcessoDecisaoST()
    {
        m_listaDecisoes = new ArrayList<>();
                
    }
    
    public void setMecanismoDecisao(MecanismoDecisao m) 
    {
        m_mecanismoDecisao = m;
    }
    
    public void decide() 
    {
        List<Decisao> ld= m_mecanismoDecisao.decide((ProcessoDecisao) this);
    }

    public void setListDecisoes(List<Decisao> ld) 
    {
        m_listaDecisoes = ld;
    }

    public Decisao novaDecisao() 
    {
        return new Decisao();
    }

    public boolean notifica() 
    {
              
        for( ListIterator<Decisao> it = m_listaDecisoes.listIterator(); it.hasNext(); )
        {
            Decisao d = it.next();
            d.notifica();
        }
        return true;
    }
    
    public List<Decisao> getDecisoes() 
    {
        return m_listaDecisoes ;   
    }
  
}
