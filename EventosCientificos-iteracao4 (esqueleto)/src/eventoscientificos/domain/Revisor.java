/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

/**
 *
 * @author Nuno Silva
 */
public class Revisor
{
    private String m_strNome;
    private Utilizador m_utilizador;
    
    public Revisor(Utilizador u)
    {
        m_strNome = u.getNome();
        m_utilizador = u;
    }

    public boolean valida()
    {
        System.out.println("Organizador:valida: " + this.toString());
        return true;
    }

}
