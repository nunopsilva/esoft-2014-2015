/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class RegistoEventos
{
    private List<Evento> m_listaEventos;
    
    public RegistoEventos()
    {
        m_listaEventos = new ArrayList<Evento>(); 
    }
    
    public Evento novoEvento()
    {
        return new Evento();
    }

    public boolean registaEvento(Evento e)
    {
        if( e.valida() && validaEvento(e) )
            return addEvento(e);
        else
            return false;
    }
    
    private boolean addEvento(Evento e)
    {
        e.setStateRegistado();
        return m_listaEventos.add(e);
    }
    
    private boolean validaEvento(Evento e)
    {
        System.out.println("RegistoEventos: validaEvento:" + e.toString());
        return true;
    }

    public List<Decidivel> getDecisiveis(String strID) 
    {
        List<Decidivel> ld = new ArrayList<>();
        for(Evento e:this.m_listaEventos)
        {
            ListaOrganizadores lorg = e.getListaOrganizadores();
            if (e.isInEmDecisao() && lorg.hasOrganizador(strID))
                ld.add(e);
            
           ld.addAll(e.getListaDeSessõesTemáticas().getDecisiveis(strID));
        }
        return ld;
    }
    
    public List<CPDefinivel> getListaCPDefiniveisEmDefinicaoDoUtilizador(String strID)
    {
        List<CPDefinivel> ls = new ArrayList<CPDefinivel>();
        for(Evento e:this.m_listaEventos)
        {
            ListaOrganizadores lorg = e.getListaOrganizadores();
            if (e.isInSTDefinidas() && lorg.hasOrganizador(strID))
                ls.add(e);
            
           ls.addAll(e.getListaDeSessõesTemáticas().getListaCPDefiniveisEmDefinicaoDoUtilizador(strID));
        }
        return ls;
    }

    public List<Evento> getListaEventosRegistadosDoUtilizador(String strID)
    {
        List<Evento> ls = new ArrayList<Evento>();
        for(Evento e:this.m_listaEventos)
        {
            ListaOrganizadores lorg = e.getListaOrganizadores();
            if (e.isInRegistado() && lorg.hasOrganizador(strID))
                ls.add(e);
        }
        return ls;

    }

    public List<Submissivel> getListaSubmissiveisEmSubmissao()
    {
        List<Submissivel> ls = new ArrayList<Submissivel>();
        for(Evento e:this.m_listaEventos)
        {
            if (e.isInEmSubmissao())
                ls.add(e);
            
           ls.addAll(e.getListaDeSessõesTemáticas().getListaSubmissiveisEmSubmissao());
        }
        return ls;
    }

    public List<Revisavel> getRevisaveisEmRevisaoDoRevisor(String strID) 
    {
        List<Revisavel> lr = new ArrayList<>();
        for(Evento e:this.m_listaEventos)
        {
            
            if (e.isInEmRevisao() && e.hasRevisor(strID))
                lr.add(e);
            
           lr.addAll(e.getListaDeSessõesTemáticas().getRevisaveis(strID));
        }
        return lr;
    }
}
