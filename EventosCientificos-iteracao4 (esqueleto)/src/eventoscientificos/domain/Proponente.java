/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.domain;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Proponente
{
    private final String m_strNome;
    private Utilizador m_utilizador;

    public Proponente(Utilizador u )
    {
       m_strNome = u.getNome();
       this.setUtilizador(u);
    }

    private void setUtilizador(Utilizador u)
    {
        m_utilizador = u;
    }
    
    public Utilizador getUtilizador()
    {
        return this.m_utilizador;
    }
    
    public boolean valida()
    {
        System.out.println("Proponente:valida: " + this.toString());
        return true;
    }
}
