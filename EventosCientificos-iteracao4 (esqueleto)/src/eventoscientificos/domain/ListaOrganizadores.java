/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class ListaOrganizadores
{
    private List<Organizador> m_listaOrganizadores;
    
    public ListaOrganizadores()
    {
        m_listaOrganizadores = new ArrayList<Organizador>(); 
    }
    
    public boolean addOrganizador(Utilizador u)
    {
       if (u== null)
           return false;
       System.out.println("ListaOrganizadores: addOrganizador: " + u.toString());
       Organizador o = new Organizador(u);
        
       if( o.valida() && validaOrganizador(o) )
            return addOrganizador(o);
        else
            return false;
    }
    
    private boolean addOrganizador(Organizador o)
    {
        return m_listaOrganizadores.add(o);
    }

    private boolean validaOrganizador(Organizador o)
    {
        System.out.println("ListaOrganizadores: validaOrganizadore:" + o.toString());
        return true;
    }

    public boolean hasOrganizador(String strID)
    {
        for(Organizador org:this.m_listaOrganizadores)
        {
            if (org.getUtilizador().getUsername().equals(strID))
                return true;
        }
        return false;
    }
}
