/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import eventoscientificos.state.EventoState;
import eventoscientificos.state.EventoStateCriado;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Nuno Silva
 */
public class Evento implements CPDefinivel, Submissivel, Distribuivel, Decidivel, Licitavel, Revisavel
{
    private String m_strTitulo;
    private String m_strDescricao;
    private Local m_local;
    private Date m_strDataInicio;
    private Date m_strDataFim;
    private Date m_strDataInicioSubmissao;
    private Date m_strDataFimSubmissao;
    private Date m_strDataInicioDistribuicao;
    private EventoState m_state;
    private CP m_cp;
    private ListaOrganizadores m_lstOrganizadores;
    private ListaSessoesTematicas m_lstSessoesTematicas;
    private ListaSubmissoes m_lstSubmissoes;
    private ProcessoDecisao m_processoDecisao;
    private ProcessoLicitacao m_processoLicitacao;
    private ProcessoDistribuicao m_processoDistribuicao;

    public Evento()
    {
        new EventoStateCriado(this);
        m_local = new Local();
        m_lstOrganizadores = new ListaOrganizadores();
        m_lstSessoesTematicas =new ListaSessoesTematicas(this);
        m_lstSubmissoes =new ListaSubmissoes(this);
        
    }
    
    public boolean setState(EventoState state)
    {
        this.m_state = state;
        return true;
    }
    
    @Override
    public CP novaCP()
    {
        m_cp = new CPEvento();
        
        return m_cp;
    }
    
    @Override
    public boolean setCP(CP cp)
    {
        m_cp = cp;
        return true;
    }
    
    public boolean isInRegistado()
    {
        return this.m_state.isInRegistado();
    }
    public boolean isInSTDefinidas()
    {
        return this.m_state.isInSTDefinidas();
    }
    public boolean isInCPDefinida()
    {
        return this.m_state.isInCPDefinida();
    }
    public boolean isInEmSubmissao()
    {
        return this.m_state.isInEmSubmissao();
    }
    public boolean isInEmDetecaoConflitos()
    {
        return this.m_state.isInEmDetecaoConflitos();
    }
    public boolean isInEmLicitacao()
    {
        return this.m_state.isInEmLicitacao();
    }
    public boolean isInEmDistribuicao()
    {
        return this.m_state.isInEmDistribuicao();
    }
    public boolean isInEmRevisao()
    {
        return this.m_state.isInEmRevisao();
    }
    public boolean isInEmDecisao()
    {
        return this.m_state.isInEmDecisao();
    }
    public boolean isInEmDecidido()
    {
        return this.m_state.isInEmDecidido();
    }
    
    public boolean setStateRegistado()
    {
        return this.m_state.setStateRegistado();
    }
    public boolean setStateSTDefinidas()
    {
        return this.m_state.setStateSTDefinidas();
    }
    public boolean setStateEmSubmissao()
    {
        return this.m_state.setStateEmSubmissao();
    }
    public boolean setStateEmDetecaoConflitos()
    {
        return this.m_state.setStateEmDetecaoConflitos();
    }
    public boolean setStateEmLicitacao()
    {
        return this.m_state.setStateEmLicitacao();
    }
    public boolean setStateEmDistribuicao()
    {
        return this.m_state.setStateEmDistribuicao();
    }
    public boolean setStateEmRevisao()
    {
        return this.m_state.setStateEmRevisao();
    }
    public boolean setStateEmDecisao()
    {
        return this.m_state.setStateEmDecisao();
    }
    public boolean setStateEmDecidido()
    {
        return this.m_state.setStateEmDecidido();
    }
    
    
    public String getTitulo()
    {
        return this.m_strTitulo;
    }
    
    public Date getDataInicioSubmissao()
    {
        return this.m_strDataInicioSubmissao;
    }
    
    public Date getDataFimSubmissao()
    {
        return this.m_strDataFimSubmissao;
    }
    
    public Date getDataInicioDistribuicao()
    {
        return this.m_strDataInicioDistribuicao;
    }
    
    
    public ListaOrganizadores getListaOrganizadores()
    {
        return this.m_lstOrganizadores;
    }
    
    public void setTitulo(String strTitulo)
    {
        this.m_strTitulo = strTitulo;
    }

    public void setDescricao(String strDescricao)
    {
        this.m_strDescricao = strDescricao;
    }

    public void setDataInicio(Date strDataInicio)
    {
        this.m_strDataInicio = strDataInicio;
    }

    public void setDataFim(Date strDataFim)
    {
        this.m_strDataFim = strDataFim;
    }
    
    public void setDataInicioSubmissao(Date strDataInicioSubmissao)
    {
        this.m_strDataInicioSubmissao = strDataInicioSubmissao;
    }
    
    public void setDataFimSubmissao(Date strDataFimSubmissao)
    {
        this.m_strDataFimSubmissao = strDataFimSubmissao;
    }
    
    public void setDataInicioDistribuicao(Date strDataInicioDistribuicao)
    {
        this.m_strDataInicioDistribuicao = strDataInicioDistribuicao;
    }

    public void setLocal(String strLocal)
    {
        m_local.setLocal(strLocal);
    }

    public boolean valida()
    {
        return this.m_state.valida();
    }

    @Override
    public String toString()
    {
        return this.m_strTitulo + "+ ..."; 
    }

    public ListaSessoesTematicas getListaDeSessõesTemáticas() {
           return this.m_lstSessoesTematicas;
    }
    
    public ProcessoDecisao novoProcessoDecisao() {
        return new ProcessoDecisaoEvento();
    }

    
    public boolean setPD(ProcessoDecisao pd) {
        this.m_processoDecisao=pd;
        return true;
    }

    @Override
    public ListaSubmissoes getListaSubmissoes()
    {
        return this.m_lstSubmissoes;
    }
    
    @Override
    public ProcessoLicitacao getProcessoLicitacao() 
    {
        return m_processoLicitacao;
    }
    
    @Override
    public void setProcessoLicitacao(ProcessoLicitacao pl) 
    {
        if (pl.valida())
        {
            m_processoLicitacao = pl;
            m_state.setStateEmLicitacao();
        }
    }
    
    @Override
    public List<Submissao> getSubmissoes() 
    {
        return m_lstSubmissoes.getSubmissoes();
    }
    
    @Override
    public List<Revisor> getRevisores() 
    {
        return m_cp.getRevisores();
    }
    
    @Override
    public ProcessoLicitacao iniciaDetecao() {
        m_state.setStateEmDetecaoConflitos();
        return new ProcessoLicitacaoEvento();
    }

    @Override
    public ProcessoDistribuicao getProcessoDistribuicao()
    {
        return m_processoDistribuicao;
    }

    

    boolean hasRevisor(String strID) 
    {
        return false;
    }

    @Override
    public void alteraParaEmDecisão() {
        ListaRevisoes lr=m_processoDistribuicao.getListaDeRevisoes();
        if (lr.isRevisoesConcluidas())
            setEmDecisao();
        
    }

    private boolean setEmDecisao() 
    {
        return this.m_state.setStateEmDecisao();
    }
    


}
