/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class ListaAutores
{
    private List<Autor> m_listaAutores;
    public ListaAutores()
    {
        m_listaAutores = new ArrayList<Autor>();
    }
    
    public Autor novoAutor(String strNome, String strAfiliacao)
    {
        Autor autor = new Autor();
        
        autor.setNome(strNome);
        autor.setAfiliacao(strAfiliacao);
        
        return autor;
    }
    
    public boolean addAutor(Autor autor)
    {
        if (validaAutor(autor))
            return m_listaAutores.add(autor);
        else
            return false;
    
    }

    private boolean validaAutor(Autor autor) 
    {
        return autor.valida();
    }
    
    protected List<Autor> getPossiveisAutoresCorrespondentes()
    {
        List<Autor> la = new ArrayList<Autor>();
        
        for(Autor autor:this.m_listaAutores)
        {
           if (autor.podeSerCorrespondente())
           {
               la.add(autor);
           }    
        }
        return la;
    }
}
