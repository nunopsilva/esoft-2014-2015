package eventoscientificos.domain;

import java.util.List;

/**
 *
 * @author Nuno Silva
 */

public interface CP
{
    public Revisor novoMembroCP(Utilizador u);
    public boolean addMembroCP(Revisor r);
    public List<Revisor> getRevisores();
}