/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class ListaSubmissoes
{
    private Submissivel m_submissivel;
    private List<Submissao> m_lstSubmissoes;
    public ListaSubmissoes(Submissivel est)
    {
        this.m_submissivel = est;
        this.m_lstSubmissoes = new ArrayList<Submissao>();
    }
    
    public Submissao novaSubmissao() {
        return new Submissao();
    }
    
    public boolean addSubmissao(Submissao submissao)
    {
        if (validaSubmissao(submissao))
        {
            return this.m_lstSubmissoes.add(submissao);
        }
        else
            return false;
    }
    
    public List<Submissao> getSubmissoes()
    {
        return m_lstSubmissoes;
    }

    private boolean validaSubmissao(Submissao submissao) 
    {
        System.out.println("ListaSubmissoes:validaSubmissao");
        return submissao.valida();
    }
}
