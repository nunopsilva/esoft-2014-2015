/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class CPEvento implements CP
{
    private List<Revisor> m_listaRevisor;
    
    public List<Revisor> getRevisores()
    {
        return m_listaRevisor;
    }
            
    
    
    
    public CPEvento()
    {
        m_listaRevisor = new ArrayList<Revisor>(); 
    }
       
    
    @Override
    public Revisor novoMembroCP(Utilizador u )
    {
        Revisor r = new Revisor(u);
        
        if( r.valida() && validaMembroCP(r) )
            return r;
        else
            return null;
    }
    
    private boolean validaMembroCP(Revisor r)
    {
        System.out.println("CPEvento: validaMembroCP:" + r.toString());
        return true;
    }
    
    @Override
    public boolean addMembroCP(Revisor r)
    {
        if (r== null)
           return false;
        System.out.println("CPEvento: registaMembroCP: " + r.toString());
       

       if( r.valida() && validaMembroCP(r) )
            return m_listaRevisor.add(r);
        else
            return false;
    }

      

    
}
