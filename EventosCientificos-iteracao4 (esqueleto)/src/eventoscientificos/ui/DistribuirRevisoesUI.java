/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controllers.DistribuirRevisoesController;
import eventoscientificos.domain.Empresa;

/**
 *
 * @author nunosilva
 */
class DistribuirRevisoesUI implements UI
{
    private Empresa m_empresa;
    private DistribuirRevisoesController m_controller;

    public DistribuirRevisoesUI( Empresa empresa )
    {
        m_empresa = empresa;
        m_controller = new DistribuirRevisoesController(m_empresa);
    }

    public void run()
    {

    }
}
