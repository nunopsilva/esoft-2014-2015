package eventoscientificos.ui;

import eventoscientificos.controllers.CriarEventoController;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.Utilizador;
import java.util.Date;
import utils.Utils;

/**
 *
 * @author Nuno Silva
 */

public class CriarEventoUI implements UI
{
    private Empresa m_empresa;
    private CriarEventoController m_controller;

    public CriarEventoUI( Empresa empresa )
    {
        m_empresa = empresa;
        m_controller = new CriarEventoController(m_empresa);
    }

    public void run()
    {
        novoEvento();

        Evento evento = introduzDadosEvento();

        apresentaEvento( evento );
    }

    private void novoEvento()
    {
        m_controller.novoEvento();
    }

    private Evento introduzDadosEvento()
    {
        String strTitulo = Utils.readLineFromConsole("Introduza Titulo: ");
        m_controller.setTitulo(strTitulo);

        String strDescricao = Utils.readLineFromConsole("Introduza Descricao: ");
        m_controller.setDescricao(strDescricao);

        String strLocal = Utils.readLineFromConsole("Introduza Local: ");
        m_controller.setLocal(strLocal);

        Date dtInicio = Utils.readDateFromConsole("Introduza Data Inicio: ");
        m_controller.setDataInicio(dtInicio);
        
        Date dtFim = Utils.readDateFromConsole("Introduza Data Fim: ");
        m_controller.setDataFim(dtFim);
       
        Date dtInicioSubmissao = Utils.readDateFromConsole("Introduza Data Inicio Submissao: ");
        m_controller.setDataInicioSubmissao(dtInicioSubmissao);
        
        Date dtFimSubmissao = Utils.readDateFromConsole("Introduza Data Fim Submissao: ");
        m_controller.setDataFimSubmissao(dtFimSubmissao);
        
        Date dtInicioDistribuicao = Utils.readDateFromConsole("Introduza Data Inicio Distribuicao: ");
        m_controller.setDataInicioDistribuicao(dtInicioDistribuicao);
        
        while (Utils.confirma("Pretende inserir orgaizador (s/n)?"))
        {
            String strOrg = Utils.readLineFromConsole("Introduza ID Organizador: ");
            m_controller.addOrganizador(strOrg);
        }
        
        apresentaEvento(m_controller.getEvento());

        if (Utils.confirma("Confirma?"))
           return m_controller.registaEvento();
        return null;
    }

    private void apresentaEvento( Evento evento )
    {
        if(evento == null)
            System.out.println("Evento não registado.");
        else
            System.out.println(evento.toString() );
    }
}