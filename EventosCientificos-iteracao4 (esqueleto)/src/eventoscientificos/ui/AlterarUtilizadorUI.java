/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controllers.AlterarUtilizadorController;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Utilizador;
import utils.Utils;

/**
 *
 * @author nunosilva
 */
class AlterarUtilizadorUI implements UI
{
    private Empresa m_empresa;
    private AlterarUtilizadorController m_controller;

    public AlterarUtilizadorUI( Empresa empresa )
    {
        m_empresa = empresa;
        m_controller = new AlterarUtilizadorController(m_empresa);
    }

    @Override
    public void run()
    {
        String strUser = Utils.readLineFromConsole("Introduza ID Utilizador: ");
        Utilizador u = m_controller.getUtilizador(strUser);
        
        u.toString();
        
        String strNome = Utils.readLineFromConsole("Novo Nome:").trim();
        if (strNome.isEmpty())
            strNome = u.getNome();
        String strUsername = Utils.readLineFromConsole("Novo Username:").trim();
        if (strUsername.isEmpty())
            strUsername = u.getUsername();
        String strPwd = Utils.readLineFromConsole("Nova Pwd:").trim();
        if (strPwd.isEmpty())
            strPwd = u.getPwd();
        String strEmail = Utils.readLineFromConsole("Novo Email:").trim();
        if (strEmail.isEmpty())
            strPwd = u.getEmail();
        
        if(m_controller.alteraDados(strNome, strUsername, strPwd, strEmail))
            System.out.println("Utilizador alterado com sucesso");
    }
}
