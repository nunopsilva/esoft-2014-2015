/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controllers.AlterarSubmissaoController;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Utilizador;

/**
 *
 * @author nunosilva
 */
class AlterarSubmissaoUI implements UI
{
    private Empresa m_empresa;
    private AlterarSubmissaoController m_controllerAlterarSubmissao;

    public AlterarSubmissaoUI( Empresa empresa )
    {
        m_empresa = empresa;
        m_controllerAlterarSubmissao = new AlterarSubmissaoController(m_empresa);
    }

    public void run()
    {

    }
}
