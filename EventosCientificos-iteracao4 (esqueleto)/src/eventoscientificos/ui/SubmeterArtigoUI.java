/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.ui;


import eventoscientificos.controllers.SubmeterArtigoController;
import eventoscientificos.domain.Autor;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Revisor;
import eventoscientificos.domain.Submissivel;
import java.util.List;
import utils.Utils;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class SubmeterArtigoUI  implements UI
{
    private Empresa m_empresa;
    private SubmeterArtigoController m_controller;

    public SubmeterArtigoUI( Empresa empresa )
    {
        m_empresa = empresa;
        m_controller = new SubmeterArtigoController(m_empresa);
    }  

    @Override
    public void run()
    {   
        List<Submissivel> lsObjs = m_controller.getListaSubmissiveisEmSubmissao();
        Utils.apresentaLista(lsObjs, "Selecione Evento/ST a submeter:");
        Submissivel submissivel = (Submissivel) Utils.selecionaObject(lsObjs);
        m_controller.selectSubmissivel(submissivel);
        
        String strTitulo = Utils.readLineFromConsole("Introduza Titulo do Artigo:");
        String strResumo = Utils.readLineFromConsole("Introduza Resumo do Artigo:");
        m_controller.setDados(strTitulo, strTitulo);
        
        while (Utils.confirma("Pretende inserir Autor (s/n)?"))
        {
            String strNome = Utils.readLineFromConsole("Introduza Nome do Autor: ");
            String strAfl = Utils.readLineFromConsole("Introduza Afiliacao do Autor: ");
            
            Autor aut = m_controller.novoAutor(strNome, strAfl);
            aut.toString();
            if (Utils.confirma("Confirma adicão do Autor?"))
                m_controller.addAutor(aut);
        }
        
        List<Autor> lsAutores = m_controller.getPossiveisAutoresCorrespondentes();
        Utils.apresentaLista(lsAutores, "Selecione AutorCorrespondente:");
        Autor correspondente = (Autor) Utils.selecionaObject(lsAutores);
        m_controller.setCorrespondente(correspondente);
            
        
        String strPDF = Utils.readLineFromConsole("Introduza PDF do Artigo:");
        m_controller.setFicheiro(strPDF);
        
        if (Utils.confirma("Confirma a submissão?"))
            m_controller.registarSubmissao();
          
    }
}
