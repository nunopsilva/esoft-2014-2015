/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.controllers;

import eventoscientificos.domain.Conflito;
import eventoscientificos.domain.Decisao;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Licitavel;
import eventoscientificos.domain.Licitacao;
import eventoscientificos.domain.MecanismoDetecaoConflito;
import eventoscientificos.domain.ProcessoLicitacao;
import eventoscientificos.domain.RegistoTipoConflitos;
import eventoscientificos.domain.Revisor;
import eventoscientificos.domain.Submissao;
import eventoscientificos.domain.Submissivel;
import java.util.List;
import java.util.ListIterator;
import java.util.TimerTask;
import utils.Utils;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class DetetarConflitosController extends TimerTask
{
    private Empresa m_empresa;
    private Licitavel m_e_st;
    public DetetarConflitosController(Empresa emp, Licitavel e_st)
    {
        this.m_empresa = emp;
        this.m_e_st = e_st;
    }

    @Override
    public void run()
    {
        ProcessoLicitacao  pl=m_e_st.iniciaDetecao();
        RegistoTipoConflitos rc=m_empresa.getRegistoDeConflitos();
        List<Conflito> ltc=rc.getListaConflitos();
        List<Submissao> ls=m_e_st.getSubmissoes();
        
        for( ListIterator<Submissao> itS = ls.listIterator(); itS.hasNext(); )
        {
            Submissao s = itS.next();
            List<Revisor> lr=m_e_st.getRevisores();
            for( ListIterator<Revisor> itR = lr.listIterator(); itR.hasNext(); )
            {
                Revisor r = itR.next();
                Licitacao l=pl.novaLicitação(r,s);
                for( ListIterator<Conflito> itC = ltc.listIterator(); itC.hasNext(); )
                {
                    Conflito c = itC.next();
                    MecanismoDetecaoConflito m=c.getMecanismo();
                    m.detetarConflito(pl, l, c);
                }
            }
        }
        m_e_st.setProcessoLicitacao(pl);
    }
    
}
