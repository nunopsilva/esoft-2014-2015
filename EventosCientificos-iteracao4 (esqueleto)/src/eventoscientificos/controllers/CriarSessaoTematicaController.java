/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.domain.AlterarStateParaEmDistribuicao;
import eventoscientificos.domain.AlterarStateParaEmSubmissao;
import eventoscientificos.domain.CPDefinivel;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.ListaSessoesTematicas;
import eventoscientificos.domain.Proponente;
import eventoscientificos.domain.RegistoEventos;
import eventoscientificos.domain.RegistoUtilizadores;
import eventoscientificos.domain.SessaoTematica;
import eventoscientificos.domain.Utilizador;
import java.util.Date;
import java.util.List;

/**
 *
 * @author nunosilva
 */
public class CriarSessaoTematicaController
{
    private Empresa m_empresa;
    private RegistoEventos m_regEventos;
    private RegistoUtilizadores m_regUtilizadores;
    private Evento m_evento;
    private SessaoTematica m_st;
    private Proponente m_prop;
    public CriarSessaoTematicaController(Empresa empresa)
    {
        m_empresa = empresa;
        m_regEventos = empresa.getRegistoEventos();
        m_regUtilizadores = empresa.getRegistoUtilizadores();
    }

    public List<Evento> getListaEventosRegistadosDoUtilizador(String strID)
    {
        return m_regEventos.getListaEventosRegistadosDoUtilizador(strID);
    }
    
    public void setEvento(Evento e)
    {
        this.m_evento = e;
    }
    
    public void setDados(String cod, String desc, Date dtInicioSub, Date dtFimSub, Date dtInicioDistr)
    {
        ListaSessoesTematicas lsST = this.m_evento.getListaDeSessõesTemáticas();
        m_st = lsST.novaSessaoTematica(cod, desc,dtInicioSub,dtFimSub,dtInicioDistr);
    }
    
    public boolean addProponente(String strId)
    {
        Utilizador u = m_regUtilizadores.getUtilizadorByID(strId);
        
        m_prop =  m_st.getListaProponentes().novoProponente( u );

        return (m_prop!=null);
    }
    
    public boolean registaProponente()
    {
        return m_st.getListaProponentes().registaProponente(m_prop);
    }
    
    public SessaoTematica registaSessaoTematica()
    {
        if (m_evento.getListaDeSessõesTemáticas().registaSessaoTematica(m_st))
        {
            createTimers();
            return m_st;
        }
        return null;
    }
    
    private void createTimers()
    {
        AlterarStateParaEmSubmissao task1 = new AlterarStateParaEmSubmissao(m_st);
        this.m_empresa.schedule(task1, m_evento.getDataInicioSubmissao());
        
        AlterarStateParaEmDistribuicao task2 = new AlterarStateParaEmDistribuicao(m_st);
        this.m_empresa.schedule(task2, m_evento.getDataInicioDistribuicao());
        
        DetetarConflitosController task3 = new DetetarConflitosController(m_empresa, m_st);
        this.m_empresa.schedule(task3, m_evento.getDataFimSubmissao());
    }

    public SessaoTematica getSessaoTematica()
    {
        return this.m_st;
    }
}
