/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.state;

import eventoscientificos.domain.SessaoTematica;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class STStateEmDetecaoConflitos extends STStateImpl
{

    public STStateEmDetecaoConflitos(SessaoTematica st)
    {
        super(st);
    }
    
    @Override
    public boolean valida()
    {
        System.out.println("STStateEmDetecaoConflitos: valida:" + getSessaoTematica().toString());
        return true;
    }

    @Override
    public boolean setStateEmDetecaoConflitos()
    {
        return true;
    }

    @Override
    public boolean isInEmDetecaoConflitos()
    {
        return true;
    }
    
    @Override
    public boolean setStateEmLicitacao()
    {
        if (valida())
            return getSessaoTematica().setState(new STStateEmLicitacao(getSessaoTematica()));
        return false;
    }
    
}
