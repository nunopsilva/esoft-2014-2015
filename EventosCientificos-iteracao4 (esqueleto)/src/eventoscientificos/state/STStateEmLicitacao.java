/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.state;

import eventoscientificos.domain.SessaoTematica;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class STStateEmLicitacao extends STStateImpl
{

    public STStateEmLicitacao(SessaoTematica st)
    {
        super(st);
    }
    
    @Override
    public boolean valida()
    {
        System.out.println("STStateEmLicitacao: valida:" + getSessaoTematica().toString());
        return true;
    }

    @Override
    public boolean setStateEmLicitacao()
    {
        return true;
    }

    @Override
    public boolean isInEmLicitacao()
    {
        return true;
    }
    
    @Override
    public boolean setStateEmDistribuicao()
    {
        if (valida())
            return getSessaoTematica().setState(new STStateEmDistribuicao(getSessaoTematica()));
        return false;
    }
    
}
