/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.state;

import eventoscientificos.domain.Evento;

/**
 *
 * @author iazevedo
 */
public class EventoStateEmDecisao extends EventoStateImpl {
   
    
    public EventoStateEmDecisao(Evento e)
    {
        super(e);
    }

    @Override
    public boolean valida()
    {
        System.out.println("EventoStateEmDecisao: valida:" + getEvento().toString());
        return true;
    }
    

    @Override
    public boolean isInEmDecisao()
    {
        return true;
    }
    
    @Override
    public boolean setStateEmDecisao()
    {
        return true;
    }
    
    @Override
    public boolean setStateEmDecidido()
    {
        if (valida())
            return getEvento().setState(new EventoStateEmDecidido(getEvento()));
        return false;
    }
    
}
