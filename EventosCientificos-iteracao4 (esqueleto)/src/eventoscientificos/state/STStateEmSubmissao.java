/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.state;

import eventoscientificos.domain.SessaoTematica;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class STStateEmSubmissao extends STStateImpl
{

    public STStateEmSubmissao(SessaoTematica st)
    {
        super(st);
    }
    
    @Override
    public boolean valida()
    {
        System.out.println("STStateEmSubmissao: valida:" + getSessaoTematica().toString());
        return true;
    }

    @Override
    public boolean setStateEmSubmissao()
    {
        return true;
    }

    @Override
    public boolean isInEmSubmissao()
    {
        return true;
    }
    
    @Override
    public boolean setStateEmDetecaoConflitos()
    {
        if (valida())
            return getSessaoTematica().setState(new STStateEmDetecaoConflitos(getSessaoTematica()));
        return false;
    }
    
}
