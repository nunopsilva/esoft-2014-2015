/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.state;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public interface STState
{
    
    boolean valida();
    boolean isInRegistado();
    boolean isInCPDefinida();
    boolean isInEmSubmissao();
    boolean isInEmDetecaoConflitos();
    boolean isInEmLicitacao();
    boolean isInEmDistribuicao();
    boolean isInEmRevisao();
    boolean isInEmDecisao();
    boolean isInEmDecidido();
    
    boolean setStateRegistado();
    boolean setStateCPDefinida();
    boolean setStateEmSubmissao();
    boolean setStateEmDetecaoConflitos();
    boolean setStateEmLicitacao();
    boolean setStateEmDistribuicao();
    boolean setStateEmRevisao();
    boolean setStateEmDecisao();
    boolean setStateEmDecidido();

    
    
}
