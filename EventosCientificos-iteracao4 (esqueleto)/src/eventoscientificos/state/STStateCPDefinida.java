/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.state;

import eventoscientificos.domain.SessaoTematica;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class STStateCPDefinida extends STStateImpl
{

    public STStateCPDefinida(SessaoTematica st)
    {
        super(st);
    }
    
    @Override
    public boolean valida()
    {
        System.out.println("STStateCPDefinida: valida:" + getSessaoTematica().toString());
        return true;
    }

    @Override
    public boolean setStateCPDefinida()
    {
        return true;
    }

    @Override
    public boolean isInCPDefinida()
    {
        return true;
    }
    
    @Override
    public boolean setStateEmSubmissao()
    {
        if (valida())
            return getSessaoTematica().setState(new STStateEmSubmissao(getSessaoTematica()));
        return false;
    }
    
}
