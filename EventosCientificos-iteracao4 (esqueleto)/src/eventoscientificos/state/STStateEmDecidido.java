/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.state;

import eventoscientificos.domain.SessaoTematica;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class STStateEmDecidido extends STStateImpl
{

    public STStateEmDecidido(SessaoTematica st)
    {
        super(st);
    }
    
    @Override
    public boolean valida()
    {
        System.out.println("STStateEmDecidido: valida:" + getSessaoTematica().toString());
        return true;
    }

    @Override
    public boolean setStateEmDecidido()
    {
        return true;
    }

    @Override
    public boolean isInEmDecidido()
    {
        return true;
    }
    
}
