/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.state;

import eventoscientificos.domain.SessaoTematica;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class STStateEmRevisao extends STStateImpl
{

    public STStateEmRevisao(SessaoTematica st)
    {
        super(st);
    }
    
    @Override
    public boolean valida()
    {
        System.out.println("STStateEmRevisao: valida:" + getSessaoTematica().toString());
        return true;
    }

    @Override
    public boolean setStateEmRevisao()
    {
        return true;
    }

    @Override
    public boolean isInEmRevisao()
    {
        return true;
    }
    
    @Override
    public boolean setStateEmDecisao()
    {
        if (valida())
            return getSessaoTematica().setState(new STStateEmDecisao(getSessaoTematica()));
        return false;
    }
    
}
