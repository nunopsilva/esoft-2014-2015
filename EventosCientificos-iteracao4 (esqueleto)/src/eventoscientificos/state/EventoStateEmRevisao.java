/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.state;

import eventoscientificos.domain.Evento;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class EventoStateEmRevisao extends EventoStateImpl
{

    public EventoStateEmRevisao(Evento e)
    {
       super(e);
    }

    @Override
    public boolean valida()
    {
        System.out.println("EventoStateEmRevisao: valida:" + getEvento().toString());
        return true;
    }
    

    @Override
    public boolean isInEmRevisao()
    {
        return true;
    }
    
    @Override
    public boolean setStateEmRevisao()
    {
        return true;
    }
    
    @Override
    public boolean setStateEmDecisao()
    {
        if (valida())
            return getEvento().setState(new EventoStateEmDecisao(getEvento()));
        return false;
    }
    
}
